//
//  ArtistManager.h
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistManager : NSObject

@property NSArray *artistData;
@property NSArray *albumData;
//store the artists in a dictionary so later it's easier and faster to match them with their albums
@property NSMutableDictionary *artists;


+ (instancetype)sharedManager;

-(void) downloadData;
-(void) populateArtistsArray;

#define ARTIST_MANAGER [ArtistManager sharedManager] //singleton

@end

