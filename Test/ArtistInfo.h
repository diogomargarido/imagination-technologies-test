//
//  ArtistInfo.h
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistInfo : NSObject

@property (strong, nonatomic) UIImage *artistThumbnail;
@property (nonatomic) NSString *artistName;
@property (nonatomic) NSString *artistId;
@property (nonatomic) NSString *artistPicUrl;
@property (nonatomic) NSString *artistDescription;
@property (nonatomic) NSString *artistGenres;
@property (nonatomic) NSMutableArray *artistAlbums;

-(instancetype) initWithDictionary:(NSDictionary *) dictionary;

-(void) addAlbumWithDictionary:(NSDictionary *) dictionary;

@end


