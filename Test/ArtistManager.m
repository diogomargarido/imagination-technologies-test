//
//  ArtistManager.m
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#define JSON_URL @"http://i.img.co/data/data.json"

#import "ArtistManager.h"
#import "ArtistInfo.h"


@implementation ArtistManager

#pragma mark - Managing the detail item

+ (instancetype) sharedManager {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void) downloadData
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:JSON_URL]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               // The code here is executed when the response from the request comes back.
                               // The variable "data" will contain the response from the data.
                               // So you can do something like:
                               NSDictionary *dataDictionary = [NSJSONSerialization
                                                               JSONObjectWithData:data options:0 error:&error];
                               
                               
                               _artistData = [dataDictionary objectForKey:@"artists"];
                               
                               _albumData = [dataDictionary objectForKey:@"albums"];
                               
                               [self populateArtistsArray];
                               
                               [self populateAlbums];
                               
                           }];
}

-(void)populateArtistsArray
{
    _artists = [[NSMutableDictionary alloc] init];
    for (NSDictionary* artistKey in _artistData)
    {
        ArtistInfo* artist = [[ArtistInfo alloc] initWithDictionary:artistKey];
        [_artists setObject:artist forKey:artist.artistId];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataLoaded" object:nil];
}

-(void)populateAlbums
{
    for (NSDictionary* album in _albumData)
    {
        ArtistInfo* artist = [_artists objectForKey:[album objectForKey:@"artistId"]];
        if(artist)
            [artist addAlbumWithDictionary:album];
    }
}

@end
