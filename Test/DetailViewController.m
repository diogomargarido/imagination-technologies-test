//
//  DetailViewController.m
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import "DetailViewController.h"

#define kArtist

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        self.title = [self.detailItem artistName];
        
        
        UIImage* artistPic = [self.detailItem artistThumbnail];
        
        //manually scaling the image. ugh. (reasons detailed below)
        float horizontalScale = artistPic.size.width/self.artistImage.frame.size.width;
        float verticalScale = artistPic.size.height/self.artistImage.frame.size.height;

        UIImage *scaledImage =
        [UIImage imageWithCGImage:[artistPic CGImage]
                            scale:(artistPic.scale * MAX(horizontalScale,verticalScale))
                      orientation:(artistPic.imageOrientation)];
        
       
        self.artistImage.image = scaledImage;
        
        //NOTE: for some reason subviews are not being resized automatically as defined in the storyboard... and even when doing all these operations manually. thus the 'forced' image scaling u see above. it's just a workaround, I'd avoid those operations for efficiency purposes (even though it's just one image at a time, whenever you enter the artist detail menu, so it's not as bad in practical terms as it is theoretically
        /*
        self.artistImage.clipsToBounds = YES;
        self.artistImage.autoresizesSubviews = YES;
        self.artistImage.contentMode = UIViewContentModeScaleAspectFit;
        self.artistImage.frame = self.artistImage.frame; //reassign the view's size to itself just so it updates its subviews based on contentMode
         */
        self.artistDescriptionLabel.text = [self.detailItem artistDescription];
        self.artistGenresLabel.text = [@"Genres: " stringByAppendingString:[self.detailItem artistGenres]];
        
        if([self.detailItem.artistAlbums count]<1)
            self.albumsTitle.hidden = YES;
        float yPosition = self.albumsTitle.frame.origin.y + self.albumsTitle.frame.size.height + 125;
        for(AlbumInfo* album in self.detailItem.artistAlbums)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2-120, yPosition,240, 240)];
            imgView.image = album.albumThumbnail;
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            [self.view addSubview:imgView];
            
            yPosition += 250;
            
            UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, yPosition - self.view.bounds.size.height/2, self.view.bounds.size.width, self.view.bounds.size.height)];
            fromLabel.text = album.albumName;
            fromLabel.numberOfLines = 1;
            fromLabel.backgroundColor = [UIColor clearColor];
            fromLabel.textColor = [UIColor blackColor];
            fromLabel.textAlignment = NSTextAlignmentCenter;
            [self.view addSubview:fromLabel];
            
            yPosition += 50;
        }
        
        
    }
}



- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    float scrollViewHeight = 0;
    for (UIView* view in self.view.subviews)
    {
        scrollViewHeight += view.frame.size.height;
    }
    
    [(UIScrollView *)self.view  setContentSize:(CGSizeMake(320, scrollViewHeight))];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
