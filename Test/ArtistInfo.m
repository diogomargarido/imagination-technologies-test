//
//  ArtistInfo.m
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import "ArtistInfo.h"
#import "AlbumInfo.h"


@implementation ArtistInfo

#pragma mark - Managing the detail item

-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if(self)
    {
        self.artistId = dictionary[@"id"];
        self.artistName =  dictionary[@"name"];
        self.artistPicUrl = dictionary[@"picture"];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.artistPicUrl]];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   // The code here is executed when the response from the request comes back.
                                   // The variable "data" will contain the response from the data.
                                   // So you can do something like:
                                   self.artistThumbnail = [UIImage imageWithData:data];
                                   
                               }];
        self.artistGenres = dictionary[@"genres"];
        self.artistDescription = dictionary[@"description"];
        self.artistAlbums = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void) addAlbumWithDictionary:(NSDictionary *) dictionary
{
    AlbumInfo* album = [[AlbumInfo alloc] initWithDictionary:dictionary];
    [self.artistAlbums addObject:album];
}

@end
