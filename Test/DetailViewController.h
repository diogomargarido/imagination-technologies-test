//
//  DetailViewController.h
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArtistInfo.h"
#import "AlbumInfo.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) ArtistInfo* detailItem;
@property (strong, nonatomic,retain) IBOutlet UIImageView *artistImage;
@property (nonatomic,retain) IBOutlet UILabel *artistDescriptionLabel;
@property (nonatomic,retain) IBOutlet UILabel *artistGenresLabel;
@property (nonatomic,retain) IBOutlet UILabel *albumsTitle;

@end

