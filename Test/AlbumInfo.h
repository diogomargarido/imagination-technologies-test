//
//  AlbumInfo.h
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumInfo : NSObject

@property (strong, nonatomic) UIImage *albumThumbnail;
@property (nonatomic) NSString *albumName;
@property (nonatomic) NSString *albumId;
@property (nonatomic) NSString *artistId; //a bit redundant
@property (nonatomic) NSString *albumPicUrl;
@property (nonatomic) NSString *albumType;

-(instancetype) initWithDictionary:(NSDictionary *) dictionary;

@end


