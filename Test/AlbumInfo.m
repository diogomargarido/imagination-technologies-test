//
//  AlbumInfo.m
//  Test
//
//  Created by Diogo on 05/05/2015.
//  Copyright (c) 2015 Diogo Margarido. All rights reserved.
//

#import "AlbumInfo.h"


@implementation AlbumInfo

#pragma mark - Managing the detail item

-(instancetype) initWithDictionary:(NSDictionary *) dictionary
{
    self = [super init];
    if(self)
    {
        self.albumId = dictionary[@"id"];
        self.artistId = dictionary[@"artistId"];
        self.albumName =  dictionary[@"title"];
        self.albumPicUrl = dictionary[@"picture"];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.albumPicUrl]];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   // The code here is executed when the response from the request comes back.
                                   // The variable "data" will contain the response from the data.
                                   // So you can do something like:
                                   self.albumThumbnail = [UIImage imageWithData:data];
                                   
                               }];
        self.albumType = dictionary[@"type"];
    }
    return self;
}

@end
